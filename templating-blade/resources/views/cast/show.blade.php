@extends('adminlte.master')

@section('content')
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Cast</h1>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Cast Film</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
            </div>
        </div>

        <div class="card bg-light d-flex flex-fill">
            <div class="card-header text-muted border-bottom-0">
              Title FIlm
            </div>
            <div class="card-body pt-0">
              <div class="row">
                <div class="col-7">
                  <h2 class="lead"><b> {{$details->nama}} </b></h2>
                  <p class="text-muted text-sm"><b>Umur: </b> {{$details->umur}} </p>
                  <p class="text-muted text-sm"><b>Bio: </b> {{$details->bio}} </p>
                </div>
                <div class="col-5 text-center">
                  <img src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="user-avatar" class="img-circle img-fluid">
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="text-right">
                <a href="#" class="btn btn-sm bg-teal">
                  <i class="fas fa-comments"></i>
                </a>
                <a href="#" class="btn btn-sm btn-primary">
                  <i class="fas fa-user"></i> View Profile
                </a>
              </div>
            </div>
          </div>


    </div>
    
</div>
<!-- /.card -->

</section>
@endsection
        
    