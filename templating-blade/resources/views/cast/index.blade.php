@extends('adminlte/master')

@section('content')
    
        <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Cast</h1>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
        <div class="card-header">
            <h3 class="card-title">Cast List</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
            </div>
        </div>
        <div class="card-body">
            @if(session('succes'))
            <div class="alert alert-success">
                {{session('succes')}}
            </div>
            @endif
            @if(session('delete'))
            <div class="alert alert-danger">
                {{session('delete')}}
            </div>
            @endif
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th colspan="3" style="text-align: center">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($reads as $key => $val) {{-- mengambil data array dari controller dgn funsi (commpact reads) kemudian d buat variabel key yg berisi array assosiatif kemudian diambil lagi isi nya dgn variabel val --}}
                        <tr>
                            <td> {{ $key+1 }} </td>
                            <td> {{$val->nama}} </td>
                            <td> {{$val->umur}} </td>
                            <td> {{$val->bio}} </td>
                            <td style="width: 20px">
                                <a href="/cast/{{$val->id}}" class="btn btn-info btn-sm">Detail</a>
                            </td>
                            <td style="width: 20px;">
                                <a href="/cast/{{$val->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                            </td>
                            <td style="width: 20px;">
                                <form action="/cast/{{$val->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7" align="center">No Post</td>
                        </tr>
                    
                    @endforelse              
                </tbody>
              </table>

              
        </div>
        <div class="card-footer">
            <a class="btn btn-primary" href="/cast/create">Create New Cast</a>
          </div>
    </div>
    <!-- /.card -->

    </section>
    <!-- /.content -->



@endsection

@push('script')
    
@endpush