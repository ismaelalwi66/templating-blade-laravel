<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $reads = DB::table('cast')->get();
                
        return view('cast.index',compact('reads'));
    }

    public function create(){
        return view('cast.create');
    }

    public function show($id){
        $details= DB::table('cast')->where('id',$id)->first();

        return view('cast.show',compact('details'));
    }

    public function edit($id){
        $details= DB::table('cast')->where('id',$id)->first();

        return view('cast.edit',compact('details'));
    }

    public function store(Request $request){

        $request -> validate([
            "nama" => 'required',
            "umur" => 'required',
            "bio" => 'required'
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('succes','Post berhasil di simpan !');
    }

    public function update($id, Request $request){

        $request -> validate([
            "nama" => 'required',
            "umur" => 'required',
            "bio" => 'required'
        ]);

        $query = DB::table('cast')->where('id',$id)->update([
            'nama'=> $request['nama'],
            'umur'=> $request['umur'],
            'bio'=> $request['bio']
        ]);

        return redirect('/cast')->with('succes','Update cast berhasil di simpan !');
    }

    public function destroy($id,Request $request){
            $query = DB::table('cast')->where('id',$id)->delete();

            return redirect('/cast')->with('delete',"Cast dengan ID: $id berhasil di hapus !");
    }

}
